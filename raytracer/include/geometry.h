#ifndef geometry_h
#define geometry_h

#include <math.h>
#include <stdio.h>

// Définition vecteur 3D
typedef struct Vec3 {
    float x,y,z;
} Point3D, Vector3D;

typedef float Scalar;


Point3D pointXYZ(float x, float y, float z);
Vector3D vectorXYZ(float x, float y, float z);

Vector3D vector(Point3D from, Point3D to);

Point3D pointPlusVector(Point3D p, Vector3D v);

Vector3D addVector(Vector3D a, Vector3D b);
Vector3D subVector(Vector3D a, Vector3D b);

Vector3D multiplyVector(Vector3D v, float a);
Vector3D divideVector(Vector3D v, float a);

Scalar dot(Vector3D a, Vector3D b);

float norm(Vector3D a);
Vector3D normalize(Vector3D a);

void printVector(Vector3D v);



#endif
