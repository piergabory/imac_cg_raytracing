#ifndef raytracer_h
#define raytracer_h

#include <stdlib.h>

#include "geometry.h"
#include "color.h"
#include "shape.h"

typedef struct ray {
    Point3D origin;
    Vector3D direction;
} Ray;

typedef struct intersection{
    Point3D location;
    Color3f value;
} Intersection;

typedef struct object {
    char type;
    Sphere s;
    Cube c;
    struct object *next;
} Object, *ObjectList;

typedef struct scene {
    ObjectList objects;
} Scene;


Ray createRay(Point3D origin, Vector3D direction);

Scene createScene();

void addSphereToScene(Scene *scene, Sphere sphere);
void addCubeToScene(Scene *scene, Cube cube);

int intersectsSphere(Ray r, Sphere s, Intersection *i);

int intersectsCube(Ray r, Cube s, Intersection *i);

int throwRayOnScene(Ray r, const Scene* s, Intersection* i);

#endif /* raytracer_h */
