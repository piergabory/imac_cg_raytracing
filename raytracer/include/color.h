#ifndef color_h
#define color_h

typedef struct color3f {
    float r,g,b;
} Color3f;


Color3f color3f(float r, float g, float b);

// addition/substraction by color
Color3f addColors(Color3f c1, Color3f c2);
Color3f subColors(Color3f c1, Color3f c2);

// product/division by color
Color3f multiplyColors(Color3f c1, Color3f c2);
Color3f divideColors(Color3f c1, Color3f c2);

// product/division by scalar
Color3f multiplyColor(Color3f c1, float s);
Color3f divideColor(Color3f c1, float s);

#endif
