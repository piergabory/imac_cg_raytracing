#ifndef shape_h
#define shape_h

#include "geometry.h"
#include "color.h"

typedef struct cube {
    Point3D min;
    Point3D max;
    Color3f color;
} Cube;

typedef struct sphere {
    Point3D origin;
    float radius;
    Color3f color;
} Sphere;

Cube createCube(Point3D min, Point3D max, Color3f color);

Sphere createSphere(Point3D origin, float radius, Color3f color);

#endif /* shape_h */
