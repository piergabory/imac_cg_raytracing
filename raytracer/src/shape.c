#include "shape.h"

Cube createCube(Point3D min, Point3D max, Color3f color) {
    Cube newCube;
    newCube.min = min;
    newCube.max = max;
    newCube.color = color;
    return newCube;
}

Sphere createSphere(Point3D origin, float radius, Color3f color) {
    Sphere newSphere;
    newSphere.origin = origin;
    newSphere.radius = radius;
    newSphere.color = color;
    return newSphere;
}
