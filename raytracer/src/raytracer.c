#include "raytracer.h"

Ray createRay(Point3D origin, Vector3D direction) {
    Ray newRay;
    newRay.origin = origin;
    newRay.direction = direction;
    return newRay;
}

Scene createScene() {
    Scene scene;
    scene.objects = NULL;
    return scene;
}

void addSphereToScene(Scene *scene, Sphere sphere) {
    ObjectList newSphere = (ObjectList)malloc(sizeof(Object));
    newSphere->type = 's';
    newSphere->s = sphere;
    newSphere->next = scene->objects;
    scene->objects = newSphere;
}

void addCubeToScene(Scene *scene, Cube cube) {
    ObjectList newSphere = (ObjectList)malloc(sizeof(Object));
    newSphere->type = 'c';
    newSphere->c = cube;
    newSphere->next = scene->objects;
    scene->objects = newSphere;
}

// @return 0 si intersection, sinon 1. Passage du point de contact dans *i
int intersectsSphere(Ray r, Sphere s, Intersection* i) {
    
    // vectors OP-> and OA->
    Vector3D op = vector(pointXYZ(0,0,0), r.origin);
    Vector3D oa = vector(pointXYZ(0,0,0), s.origin);
    
    // a,b and c from at^2 + bt + c = 0
    float a = pow(norm(r.direction),2);
    
    // 2.OP-> . (-A + d->)
    float b = dot(multiplyVector(op, 2), pointPlusVector(s.origin, r.direction));
    
    float c = pow(norm(op),2) + pow(norm(oa),2) - 2 * dot(op, oa) - pow(s.radius,2);
    
    // delta of the polynomial
    float delta = pow(b,2) - (4 * a * c);
    
    // il n'y a pas d'intersection
    if (delta < 0)
        return 0;
    
    // sinon
    //solution du polynome at^2 + bt + c = 0
    float t = (b - sqrt(delta)) / (2 * a);
    
    // application de la solution à l'équation paramétrique du rayon
    i->location.x = r.origin.x + r.direction.x * t;
    i->location.y = r.origin.y + r.direction.y * t;
    i->location.z = r.origin.z + r.direction.z * t;
    
    // passage de la couleur
    i->value = s.color;
    return 1;
}

int intersectsCube(Ray r, Cube s, Intersection* i) {
    //
    return 0;
}

int throwRayOnScene(Ray r, const Scene* scene, Intersection* i) {
    int ret = 0;
    
    // boucle dans les objets
    for (Object *o = scene->objects; o != NULL; o = o->next) {
    
        Intersection tmp;
        
        // calcule intersection si cube ou sphère
        // ret sera true si il y a une intersection parmis
        // les passages de la boucle
        switch (o->type) {
            case 's': ret |= intersectsSphere(r, o->s, &tmp); break;
            case 'c': ret |= intersectsCube(r, o->c, &tmp); break;
        }
        
        
        // calcule la distance entre la plus proche
        // intersection obtenue (i) et l'origine du rayon
        float distanceI = norm(vector(r.origin, i->location));
        
        // calcule la distance entre la nouvelle
        // intersection (tmp) et l'origine du rayon
        float distanceTmp = norm(vector(r.origin, tmp.location));
        
        // si la nouvelle 'tmp' est encore plus proche, elle écrase 'i'.
        if (distanceTmp < distanceI) {
            *i = tmp;
        }
    }
    
    return ret;
}


