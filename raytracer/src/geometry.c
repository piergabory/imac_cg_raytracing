#include "geometry.h"

// Point3D Constructor
Point3D pointXYZ(float x, float y, float z) {
    Point3D n;
    n.x = x; n.y = y; n.z = z;
    return n;
}

// Vector3D Constructor
Vector3D vectorXYZ(float x, float y, float z) {
    Vector3D n;
    n.x = x; n.y = y; n.z = z;
    return n;
}

// Vector3D Constructor from 2 points
Vector3D vector(Point3D from, Point3D to) {
    Vector3D n;
    n.x = to.x - from.x;
    n.y = to.y - from.y;
    n.z = to.z - from.z;
    return n;
}

// Translate point by vector
Point3D pointPlusVector(Point3D p, Vector3D v) {
    p.x += v.x;
    p.y += v.y;
    p.z += v.z;
    return p;
}

// Vector addition
Vector3D addVector(Vector3D a, Vector3D b) {
    a.x += b.x;
    a.y += b.y;
    a.z += b.z;
    return a;
}

// Vector substraction
Vector3D subVector(Vector3D a, Vector3D b) {
    a.x -= b.x;
    a.y -= b.y;
    a.z -= b.z;
    return a;
}

// Vector Multiplication
Vector3D multiplyVector(Vector3D v, float a) {
    v.x *= a;
    v.y *= a;
    v.z *= a;
    return v;
}

// Vector Division
Vector3D divideVector(Vector3D v, float a) {
    if (a == 0) return vectorXYZ(0,0,0);
    return multiplyVector(v, 1/a);
}

// Dot product, returns a scalar - aka number
Scalar dot(Vector3D a, Vector3D b) {
    return a.x*b.x + a.y*b.y + a.z*b.z;
}

// Vector norm alias length
float norm(Vector3D a) {
    float squaredSum = a.x*a.x + a.y*a.y + a.z*a.z;
    return sqrtf(squaredSum);
}

// return vector of norm value 1
Vector3D normalize(Vector3D v) {
    return divideVector(v, norm(v));
}

// print vector in console
void printVector(Vector3D v) {
    printf("(%.1f, %.1f, %.1f)\n", v.x, v.y, v.z);
}
