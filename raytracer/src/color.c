#include "color.h"

Color3f color3f(float r, float g, float b) {
    Color3f newColor;
    newColor.r = r;
    newColor.g = g;
    newColor.b = b;
    return newColor;
}

// addition/substraction by color
Color3f addColors(Color3f c1, Color3f c2){
    c1.r += c2.r;
    c1.g += c2.g;
    c1.b += c2.b;
    return c1;
}

Color3f subColors(Color3f c1, Color3f c2){
    c1.r -= c2.r;
    c1.g -= c2.g;
    c1.b -= c2.b;
    return c1;
}


// product/division by color
Color3f multiplyColors(Color3f c1, Color3f c2){
    c1.r *= c2.r;
    c1.g *= c2.g;
    c1.b *= c2.b;
    return c1;
}

Color3f divideColors(Color3f c1, Color3f c2){
    c1.r /= c2.r;
    c1.g /= c2.g;
    c1.b /= c2.b;
    return c1;
}


// product/division by scalar
Color3f multiplyColor(Color3f c1, float s){
    c1.r *= s;
    c1.g *= s;
    c1.b *= s;
    return c1;
}

Color3f divideColor(Color3f c1, float s){
    c1.r /= s;
    c1.g /= s;
    c1.b /= s;
    return c1;
}
