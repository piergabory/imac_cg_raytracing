#include <stdlib.h>
#include <stdio.h>

#include "raytracer.h"

int main(int argc, char** argv) {
    Intersection i1;
    
    printf("%d\n",intersectsSphere(createRay(pointXYZ(1,1,1), vectorXYZ(1,1,1)), createSphere(pointXYZ(4,4,4), 1, color3f(1.,1.,1.)) , &i1));
    
    return EXIT_SUCCESS;
}
